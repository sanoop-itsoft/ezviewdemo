ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* libs\armeabi\libCloudSDK.so => app\src\main\jniLibs\armeabi\libCloudSDK.so
* libs\armeabi\libCurl.so => app\src\main\jniLibs\armeabi\libCurl.so
* libs\armeabi\libcyassl.so => app\src\main\jniLibs\armeabi\libcyassl.so
* libs\armeabi\libDiscovery.so => app\src\main\jniLibs\armeabi\libDiscovery.so
* libs\armeabi\libdspvideomjpeg.so => app\src\main\jniLibs\armeabi\libdspvideomjpeg.so
* libs\armeabi\libMP2.so => app\src\main\jniLibs\armeabi\libMP2.so
* libs\armeabi\libMP4.so => app\src\main\jniLibs\armeabi\libMP4.so
* libs\armeabi\libmXML.so => app\src\main\jniLibs\armeabi\libmXML.so
* libs\armeabi\libNDPlayer.so => app\src\main\jniLibs\armeabi\libNDPlayer.so
* libs\armeabi\libNDRender.so => app\src\main\jniLibs\armeabi\libNDRender.so
* libs\armeabi\libNDRtmp.so => app\src\main\jniLibs\armeabi\libNDRtmp.so
* libs\armeabi\libNetDEVSDK.so => app\src\main\jniLibs\armeabi\libNetDEVSDK.so
* libs\armeabi\libNetDEVSDK_JNI.so => app\src\main\jniLibs\armeabi\libNetDEVSDK_JNI.so
* libs\armeabi\libRM_Module.so => app\src\main\jniLibs\armeabi\libRM_Module.so
* libs\armeabi\libRSA.so => app\src\main\jniLibs\armeabi\libRSA.so
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
